/* Main Javascript Object */
var appPage= {};
(function($) {
	"use strict";
	
	// Set up name space.
	var app= {
		'functions': {},
		'data': {},
		'models': {}
	};
	
	/* For debugging */
	if(false) appPage= app;

	/* Functions */

	/* Run after the document is ready */
	$(document).ready(function() {});

	/* Run after the page is finished loading */
	$(window).load(function() {});
})(jQuery.noConflict());