<?php
    include("../const.php");
    
    if(!isset($_GET['produto']))
        header('Location:../');
    
    $produtoUtm  = $_GET['produto'];
    
    $produtoName = ucfirst (str_replace('-',' ', $produtoUtm));
    
?>
<!doctype html>
<html>
<head>
    <title>Analise Suplementos</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link rel="icon" href="<?php echo BASE_URL . "/img/logos/analise-suplementos/icon/analise-suplementos.svg"; ?>" type="image/gif" sizes="16x16">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:400" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel='stylesheet' id='normalize-css' href='../css/normalize.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-css' href='../css/bootstrap-custom.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='theme-css' href='../css/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='themepage-css' href='../css/theme_pages.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='omgbrah-css' href='../css/style-table.css' media='all'/>
</head>
<body class="home page-template-default page page-id-4 front-page">
<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src='<?php echo BASE_URL . "/img/logos/analise-suplementos/original-200px/analise-suplementos.svg"; ?>' title="Potencia Masculina" alt="Potencia Masculina" class="image-header">
            </div>
        </div>
    </nav>
</header>
<section id="wrapper" role="document">
    <div id="content" role="main">
        <article class="post-4 page type-page status-publish hentry" id="post-4">
            <div class="container">
                <div id="hero">
                    <div class="row">
                        <div class="col-xs-24 col-sm-14 content-top-hero">
                            <h1>3 Termogênicos Mais Vendidos em 2019 - Aprovados pela Anvisa</h1>
                            <p><strong class="color-blue">Quais Produtos são Realmente Seguros e Eficazes para a Queimar Mais Gordura?Você Deseja obter uma Corpo Magro e Atraente?</p>
                            <p>Com tanto termogênico disponível no mercado, é extremamente difícil descobrir qual realmente funciona, não é mesmo?</p>
                            <a href="#top-rated-list" class="hidden-xs col-sm-24 button_style green_button smooth_scroll">CONFIRA OS 3 TERMOGÊNICOS MAIS VENDIDOS</a>
                        </div>
                        <div class="col-xs-24 col-sm-10 content-top-hero-img">
                            <img src="../img2/head-home_<?php echo $produtoUtm ?>.png" class="upsell-img wp-post-image" alt="" sizes="(max-width: 527px) 100vw, 527px"/>
                        </div>
                        <div class="col-xs-24 col-sm-24">
                            <a href="#top-rated-list" class="col-xs-24 hidden-sm hidden-md hidden-lg button_style green_button smooth_scroll" style="margin-bottom: 25px;">CONFIRA OS TOP 3 MELHORES EMAGRECEDORES</a>
                            <p>Como saber qual produto verdadeiramente entrega resultados reais e duradouros, de forma rápida e segura, se todos afirmam que são os melhores?</p>
                            <p>Para ajudar você a desvendar este mistério, os nossos especialistas realizaram uma grande quantidade de pesquisas e uma análise minuciosa com centenas de produtos para descobrir quais de fato, são os termogênicos mais vendidos de 2019 e ajudar você a encontrar facilmente a solução ideal para conquistar aquele corpo magro e saudável que você tanto merece.</p>
                            <p><strong>Ainda não sabe qual é o melhor termogênico para você?</strong></p>
                            <p>Então, confira os nossos critérios de avaliação, comparativo, análise e todas as informações relevantes sobre esses produtos logo abaixo.</p>
                        </div>
                    </div>
                    <div class="row" id="top_ten_criteria">
                        <div class="col-xs-24">
                            <div class="banner-box">
                                <h3>Nossos Critérios de Avaliação</h3>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-overall.png" title="" alt=""/><span class="badge_label vert_center">Classificação Geral</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-ingredients.png" title="" alt=""/><span class="badge_label vert_center">Qualidade do Ingrediente</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/anvisa.png" title="" alt=""/><span class="badge_label vert_center">Aprovado pela Anvisa</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-reputation.png" title="" alt=""/><span class="badge_label vert_center">Avaliação de Clientes</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-guarantee.png" title="" alt=""/><span class="badge_label vert_center">Garantia</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-cost.png" title="" alt=""/><span class="badge_label vert_center">Custo/Benefício</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-power.png" title="" alt=""/><span class="badge_label vert_center">Velocidade dos Resultados</span></div>
                                <div class="col-xs-12 col-sm-8"><img src="../img/icons/badge-safety.png" title="" alt=""/><span class="badge_label vert_center">Segurança do produto</span></div>
                                <div class="col-xs-12 col-sm-8 "><img src="../img/icons/badge-effects.png" title="" alt=""/><span class="badge_label vert_center">Efeitos colaterais</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container container-table-comparation">
                <div class="row row-table-comparation">
                    <div class="col-xs-24 col-sm-24 col-table-comparation">
                        <table class="comparisontable">
                            <tbody>
                            <tr>
                                <td class="td-left td-title color-blue"><strong><span >Comparação dos produtos</span></strong></td>
                                <td><a href="../pages/machomacho.php?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>"><img class="table-product-image" title="Scorpogênico" src="<?php echo BASE_URL . "/img/products/scorpogenico/2.png" ?>" alt="Scorpogênico" ></a></td>
                                <td><a href="../pages/maxpotent.php?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>"><img class="table-product-image" title="Lipo 6 Black" src="<?php echo BASE_URL . "/img/products/lipo-6-black/1.png" ?>" alt="Lipo 6 Black"  ></a></td>
                                <td><a href="https://www.cleannutrition.com.br/saude-sexual?utm_source=googleads&utm_medium=cpc&utm_campaign=<?php echo $produtoUtm; ?>"><img class="table-product-image" title="<?php echo $produtoName; ?>" src="<?php echo BASE_URL . "/img/products/{$produtoUtm}/1.png" ?>" alt="<?php echo $produtoName; ?>" ></a></td>
                            </tr>
                            <tr class="t-head">
                                <td class="td-left"><span>Nome do produto</span></td>
                                <td><span>Scorpogênico</span></td>
                                <td><span>Lipo 6 Black</span></td>
                                <td><span><?php echo $produtoName; ?></span></td>
                            </tr>
                            <tr>
                                <td class="td-left"><span>Classificação</span></td>
                                <td align="center"> 9.8/10<br>
                                    <div class="star-positioner">
                                        <div class="stars">
                                            <div class="colorbar c3" > </div>
                                            <div class="star_holder">
                                                <div class="star star-1"></div>
                                                <div class="star star-2"></div>
                                                <div class="star star-3"></div>
                                                <div class="star star-4"></div>
                                                <div class="star star-5"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td align="center">
                                    4.6/10<br>
                                    <div class="star-positioner">
                                        <div class="stars">
                                            <div class="colorbar c2" > </div>
                                            <div class="star_holder">
                                                <div class="star star-1"></div>
                                                <div class="star star-2"></div>
                                                <div class="star star-3"></div>
                                                <div class="star star-4"></div>
                                                <div class="star star-5"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td align="center">
                                    3.1/10<br>
                                    <div class="star-positioner">
                                        <div class="stars">
                                            <div class="colorbar c1"> </div>
                                            <div class="star_holder">
                                                <div class="star star-1"></div>
                                                <div class="star star-2"></div>
                                                <div class="star star-3"></div>
                                                <div class="star star-0"></div>
                                                <div class="star star-0"></div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Avaliação do Cliente</span></td>
                                <td align="center" bgcolor="#FFFFFF">98,6% (2674 votos)</td>
                                <td align="center" bgcolor="#FFFFFF">48.3% (1375 votos)</td>
                                <td align="center" bgcolor="#FFFFFF">37.6% (825 votos)</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Velocidade dos Resultados</span></td>
                                <td align="center">Extremamente Rápido</td>
                                <td align="center">Médio</td>
                                <td align="center">Lento</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Qualidade</span></td>
                                <td align="center" bgcolor="#FFFFFF">Excelente</td>
                                <td align="center" bgcolor="#FFFFFF">Bom</td>
                                <td align="center" bgcolor="#FFFFFF">Ruim</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Anvisa</span></td>
                                <td align="center">Aprovado</td>
                                <td align="center">Aprovado</td>
                                <td align="center">Aprovado</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Segurança do produto</span></td>
                                <td align="center" bgcolor="#FFFFFF">Seguro de usar</td>
                                <td align="center" bgcolor="#FFFFFF">Seguro de usar</td>
                                <td align="center" bgcolor="#FFFFFF">Seguro de usar</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Garantia</span></td>
                                <td align="center">30 Dias</td>
                                <td align="center">Não tem</td>
                                <td align="center">Não tem</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Política de devolução</span></td>
                                <td align="center" bgcolor="#FFFFFF">Livre de risco</td>
                                <td align="center" bgcolor="#FFFFFF">Não tem</td>
                                <td align="center" bgcolor="#FFFFFF">Não tem</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Preço por Porção</span></td>
                                <td align="center">R$1,98</td>
                                <td align="center">R$4,49</td>
                                <td align="center">R$5,24</td>
                            </tr>
                            <tr>
                                <td class="td-left" ><span>Risco de Esgotar Estoque</span></td>
                                <td align="center" bgcolor="#FFFFFF">Altíssimo</td>
                                <td align="center" bgcolor="#FFFFFF">Alto</td>
                                <td align="center" bgcolor="#FFFFFF">Média</td>
                            </tr>
                            <tr class="table-link-comment">
                                <td class="td-left" ><span>Velocidade de entrega</span></td>
                                <td align="center">Super Rápido</td>
                                <td align="center">Rápido</td>
                                <td align="center">Regular</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- <div id="top_diet_pill_guar"> <div class="container"> <img src="http://www.dietpilluniverse.com/wp-content/themes/dpu_theme/img/mbg-seal.png" alt="90-Day Money-Back Guarantee" title="90-Day Money-Back Guarantee"/> <h2>90-Day Guarantee <span>on Any of Our Top Diet Pills!</span></h2> <p>We're so confident you'll love any product on the top 10 list below, <b>we personally back each one with a money-back guarnatee.</b> If you don't absolutely love the product, return it within 90 days for a FULL REFUND of the purchase price. Kepp reading to discover the top 10 selling diet pills of 2018.</p></div></div>-->
            <div class="container">
                <div id="top-rated-list">
                    <h2 class="top-list">TOP 3 Melhores <span>Estimulantes</span> de <span>2018</span></h2>
                    <div class="top_product clearfix">
                        <div class="col-xs-24 col-sm-12 col-md-11">
                            <figure>
                                <a href="https://www.cleannutrition.com.br/marca/scorpogenico.html?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>">
                                    <img src="<?php echo BASE_URL . "/img/products/scorpogenico/2.png" ?>" class="upsell-img wp-post-image" alt="Scorpogênico"/>
                                </a>
                            </figure>
                            <div class="ratings">
                                <div class="relative clearfix">
                                    <div class="ratings_overlay"></div>
                                    <div class="col-xs-6">
                                        <div class="border_box">
                                            <span class="rating">9.8</span><span class="rating_label">Geral</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-18">
                                        <div class="border_box">
                                            <h2 class="text-center h2 title-box">EXCELENTE</h2>
                                            <div class="content-star-box">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-12 col-md-13 first-paragarph">
                            <a href="https://www.cleannutrition.com.br/marca/scorpogenico.html?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>">
                                <h2>#1. Scorpogênico</h2>
                            </a>
                            <span class="tagline">Alternativa Mais Potente e Segura – Resultados 3x Mais Rápidos</span>
                            <div class="review_blurb">
                                <p><a href="https://www.cleannutrition.com.br/marca/scorpogenico.html?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>" data-name="top10-upsell-blurb" class="48">Scorpogênico </a> se tornou uma das opções mais seguras e eficazes para potencializar o aumento peniano. Um poderoso estimulador que tem ajudado milhares de homens em todo o país a durarem muito mais tempo na cama com um membro maior e mais grosso. Além de promover esse crescimento rápido e responsável, contribui significativamente para relações mais duradouras e prazerosas.</p>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-24 col-md-24">
                            <div class="review_blurb">
                                <p>Scorpogênico tornou-se uma das opções mais seguras e eficazes para potencializar a queima de gordura. Um poderoso nutracêutico que tem ajudado milhares de pessoas em todo o país a perder peso e eliminar gordura teimosa (barriga, coxas e bumbum). Além de promover um emagrecimento rápido e responsável, contribui significativamente para reduzir medidas, inibir o apetite, acelerar o metabolismo e aumentar a produtividade no dia a dia.</p>
                                <p>Este é um dos poucos termogênicos disponíveis no mercado que contém compostos 100% seguros, eficazes e apoiados pela ciência.</p>
                                <p>Comprometidos com a excelência e inovação, o distribuidor oficial de Scorpogênico proporciona uma ótima alternativa aos medicamentos perigosos prescritos atualmente.</p>
                                <br><span class="tagline">DESTAQUE DO PRODUTO</span>
                                <p><strong>Fórmula Segura e Eficiente:</strong> Scorpogênico foi projetado cientificamente por especialistas à base de compostos de alta qualidade que trabalham sinergicamente no organismo e promovem uma abordagem completa e inovadora capaz de atuar diretamente sobre fatores chaves e proporcionar o aumento das taxas metabólicas, ativar o metabolismo e transformar o corpo em uma verdadeira máquina de queima de gordura, oferecendo resultados até 3x mais rápidos.</p>
                                <p><strong>Incomparável:</strong> Scorpogênico é um avanço revolucionário no mercado de nutracêuticos. Sua eficácia tem conseguido impressionar especialistas em saúde e garantir a completa satisfação de 98,6% dos seus clientes em todo o país.</p>
                                <p><strong>Nota do Editor:</strong>   Scorpogênico é um produto aprovado pela ANVISA e atende rigorosamente todas as normas vigentes e padrões de qualidade. Sua fórmula não contém nenhuma substância perigosa que possa causar efeitos colaterais nocivos e colocar em risco a sua saúde, aliás sua fórmula é 100% natural. Portanto, se você realmente deseja perder peso com total segurança, acreditamos que Scorpogênico pode ser uma excelente escolha para que você conquiste um corpo magro, bonito e atraente. E o melhor de tudo... Você pode contar com uma garantia exclusiva de 30 dias absolutamente livre de riscos, ou seja, é sua satisfação garantida ou a devolução do seu dinheiro.</p>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-12 col-md-13">
                            <div class="row cta-top-product">
                                <div class="col-xs-24 col-sm-11"><a href="https://www.cleannutrition.com.br/marca/scorpogenico.html?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>" class="button_style gray_button">Site oficial</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="top_product clearfix">
                        <div class="col-xs-24 col-sm-12 col-md-11">
                            <figure>
                                <a href="https://www.cleannutrition.com.br/marca/scorpogenico.html??utm_source=googleads&utm_medium=cpc&utm_campaign=l6b<?php echo $produtoUtm; ?>">
                                    <img src="<?php echo BASE_URL . "/img/products/lipo-6-black/1.png" ?>" class="upsell-img wp-post-image" alt="Lipo 6 Black"/>
                                </a>
                            </figure>
                            <div class="ratings">
                                <div class="relative clearfix">
                                    <div class="ratings_overlay"></div>
                                    <div class="col-xs-6">
                                        <div class="border_box"><span class="rating">4.6</span><span class="rating_label">Geral</span></div>
                                    </div>
                                    <div class="col-xs-18">
                                        <div class="border_box">
                                            <h2 class="text-center h2 title-box">ÓTIMO</h2>
                                            <div class="content-star-box"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star-half-o checked"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-12 col-md-13 first-paragarph">
                            <a href="https://www.cleannutrition.com.br/marca/scorpogenico.html??utm_source=googleads&utm_medium=cpc&utm_campaign=l6b<?php echo $produtoUtm; ?>">
                                <h2>#2.  Lipo 6 Black</h2>
                            </a>
                            <span class="tagline">Fórmula Recente no Mercado dos termogênicos - Prova de que Excesso de Promessas</span>
                            <span class="tagline">Possa Ser Apenas Lábia</span>
                            <div class="review_blurb">
                                <p><a href="https://www.cleannutrition.com.br/marca/scorpogenico.html??utm_source=googleads&utm_medium=cpc&utm_campaign=l6b<?php echo $produtoUtm; ?>" data-name="top10-upsell-blurb" class="74">Lipo 6 Black </a> é uma das soluções mais recomendadas para homens que desejam perder vergonha do seu membro, e atingir seu verdadeiro potencial. O produto já foi testado e aprovado por milhares de pessoas em todo o Brasil e em mais de 20 países, sendo o mais vendido nos últimos anos. Cerca de 92,6% de suas clientes relatam resultados de sucesso com essa poderosa fórmula estimuladora.</p>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-24 col-md-24">
                            <div class="review_blurb">
                                <p>Lipo 6 Black chegou há pouco tempo no comércio fitness, e já te dado o que falar. Ele surgiu com promessas como a de que ele age diretamente no tecido adiposo, não necessitando de muitos exercícios físicos. Nós sabemos que conquistar um corpo musculoso não é uma tarefa fácil, e que mesmo que você queira agilizar o processo, é necessário dietas e treinos musculares. Com aprovação de cerca de 48,3%, podemos perceber que sua recepção não foi tão boa pelos consumidores.</p>
                                <p>Isso é o resultado de propaganda enganosa, e um auxílio quase inexistente ao cliente. Se consolidar no mercado não é uma tarefa fácil, ainda mais na segmentação fitness, por isso é importante ser sempre verdadeiro com o consumidor, e se manter em prontidão para dúvidas e questionamentos. </p>
                                <p>A verdade é que, Lipo 6 Black tem excesso de cafeína, e poucos ingredientes que realmente acelera o metabolismo de forma com que ele te ajude a emagrecer.</p>
                                <br><span class="tagline">DESTAQUE DO PRODUTO</span>
                                <p><strong>Fórmula Diversificada:</strong>Lipo 6 Black de fato tem uma fórmula diferente no mercado, e esse pode ser o motivo de seu sucesso em tão pouco tempo no mercado. Cada composto dele é eficiente, mas infelizmente eles são dispostos no termogênico de forma errônea, já que tem muito de uns ingredientes, e pouquíssimo de outros.</p>
                                <p><strong>Composição:</strong> Quando olhamos a composição de Lipo 6 Black, encontramos um excesso de cafeína, implicando que o horário da ingestão do termogênico seja pela manhã. Usá-lo pela noite pode resultar em insônia e inquietação.</p>
                                <p><strong>Nota do Editor:</strong> Diante de tudo isso, podemos dizer que Lipo 6 Black é um termogênico emagrecedor com excesso de cafeína, podendo até prejudicar o seu rendimento ao longo do dia. A baixa aprovação do termogênico vem justamente disso. O Lipo 6 Black não possui site oficial, e os revendedores não dão garantia, ou possuem políticas de devolução. </p>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-12 col-md-13">
                            <div class="row cta-top-product">
                                <div class="col-xs-24 col-sm-11"><a href="https://www.cleannutrition.com.br/marca/scorpogenico.html??utm_source=googleads&utm_medium=cpc&utm_campaign=l6b<?php echo $produtoUtm; ?>" class="button_style gray_button">Site oficial</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="top_product clearfix">
                        <div class="col-xs-24 col-sm-12 col-md-11">
                            <figure>
                                <a href="https://www.cleannutrition.com.br/saude-sexual?utm_source=googleads&utm_medium=cpc&utm_campaign=<?php echo $produtoUtm ?>">
                                    <img src="<?php echo BASE_URL . "/img/products/{$produtoUtm}/1.png" ?>" class="upsell-img wp-post-image" alt="<?php echo $produtoName; ?>"/>
                                </a>
                            </figure>
                            <div class="ratings">
                                <div class="relative clearfix">
                                    <div class="ratings_overlay"></div>
                                    <div class="col-xs-6">
                                        <div class="border_box"><span class="rating">3.1</span><span class="rating_label">Geral</span></div>
                                    </div>
                                    <div class="col-xs-18">
                                        <div class="border_box">
                                            <h2 class="text-center h2 title-box">REGULAR</h2>
                                            <div class="content-star-box"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star-o checked"></span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-12 col-md-13 first-paragarph">
                            <a href="https://www.cleannutrition.com.br/saude-sexual?utm_source=googleads&utm_medium=cpc&utm_campaign=<?php echo $produtoUtm ?>">
                                <h2>#3. <?php echo $produtoName; ?></h2>
                            </a>
                            <span class="tagline">Fórmula Com Excesso de Composto e Sem Eficiência</span>
                            <div class="review_blurb">
                                <p><a href="https://www.cleannutrition.com.br/saude-sexual?utm_source=googleads&utm_medium=cpc&utm_campaign=<?php echo $produtoUtm ?>" data-name="top10-upsell-blurb" class="15"><?php echo $produtoName; ?> </a> mais uma vez provou ser o melhor potencializador de 2018. Trata-se de uma solução única e completa que pode promover resultados mais rápidos do que apenas gel e exercícios sozinhos. Além disso, é capaz de aumentar a testosterona livre, e produzir ácido nítrico, promovendo ereções mais longas, aumento da libido, e confiança na hora H.</p>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-24 col-md-24">
                            <div class="review_blurb">
                                <p><?php echo $produtoName; ?> já está a bastante tempo no mercado, e tem uma base consolidada em seu segmento, mas isso não implica que ele tenha qualidade. Com uma chuva de compostos sem cabimento para o seu propósito, Kimera é ineficaz, e não traz resultados.</p>
                                <p>Ao olharmos o seu site vemos uma perfeita combinação de compostos totalmente apoiados por estudos científicos e que oferecem múltiplos benefícios para a saúde, mas nós compramos e testamos, mas não tivemos sucesso com nenhum resultado.</p>
                                <p>Após semanas de uso, não obtivemos resultado algum. Pelo seu custo ser maior, não ter garantia ou política de devolução, podemos ver que as clientes não aprovam o produto graças a sua aprovação de apenas 37,6% dos votos.</p>
                                <br><span class="tagline">DESTAQUE DO PRODUTO</span>
                                <p><strong>Comprimidos:</strong>  Os dois outros termogênicos apresentados antes vêm em cápsulas, já este vem em forma de comprimido. As pessoas que o testaram para nós, reclamaram de que assim que colocado na boca, ele começava a dissolver e deixava um gosto extremamente amargo na boca.</p>
                                <p><strong>Fórmula Registrada:</strong> Um dos poucos pontos positivos de Kimera é o fato dele ser aprovado pela ANVISA e atender todas as normas vigentes. Ele também possui fórmula registrada, o que transmite ainda mais segurança na hora da compra.</p>
                                <p><strong>Nota do Editor:</strong>  <?php echo $produtoName; ?> provou ser, se não a pior, uma das piores escolhas para quem procura aumentar o metabolismo, perder massa gorda, e ter o corpo que sempre sonhou. Além de ter o custo maior que os demais produtos, ele apenas vende promessas que dificilmente serão cumpridas. </p>
                            </div>
                        </div>
                        <div class="col-xs-24 col-sm-12 col-md-13">
                            <div class="row cta-top-product">
                                <div class="col-xs-24 col-sm-11"><a href="https://www.cleannutrition.com.br/saude-sexual?utm_source=googleads&utm_medium=cpc&utm_campaign=<?php echo $produtoUtm ?>" class="button_style gray_button">Site oficial</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="top_product clearfix">
                        <div class="col-xs-24 col-sm-24 col-md-24">
                            <h1>CONCLUSÃO:</h1>
                            <br>
                            <div class="review_blurb">
                                <p>Tudo o que você precisa fazer agora é escolher o termogênico para te auxiliar nesse processo de ganho muscular.</p>
                                <p>Então, tudo o que você precisa fazer agora é escolher o seu potencializador para te auxiliar nesse processo de crescimento do músculo.</p>
                                <ul>
                                    <li><strong><a href="https://www.cleannutrition.com.br/marca/scorpogenico.html?utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>">Scorpogênico:</a></strong>  Uma excelente alternativa aos termogênicos com compostos sintéticos, e possui resultados extremamente rápidos.</li>
                                    <li><strong><a href="https://www.cleannutrition.com.br/marca/scorpogenico.html??utm_source=googleads&utm_medium=cpc&utm_campaign=sc<?php echo $produtoUtm; ?>">Lipo 6 Black:</a></strong>Excesso de cafeína, que pode acabar prejudicando com o ingere.</li>
                                    <li><strong><a href="https://www.cleannutrition.com.br/saude-sexual?utm_source=googleads&utm_medium=cpc&utm_campaign=<?php echo $produtoUtm; ?>"><?php echo $produtoName; ?></a>:</u></strong>  Não cumpre nada que promete, derrete na boca, e tem preço elevado..</li>
                                </ul>
                                <br><br><span class="tagline">Agora só depende de você!</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<footer>
    <div id="footer_wrapper">
        <div id="footer_subnav">
            <p class="copyright">
                © 2018 - <script type="text/javascript">var now=new Date(); document.write(now.getFullYear());</script> 
            </p>
        </div>
    </div>
</footer>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js'></script>
<script type='text/javascript' src='../js/devicedetect.min.js'></script>
<script type='text/javascript' src='../js/modernizr.custom.js'></script>
<script type='text/javascript' src='../js/fastclick.min.js'></script>
</body>
</html>