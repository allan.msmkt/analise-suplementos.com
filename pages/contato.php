<!doctype html>
<html>
<head>
		<title>Fale Conosco → Analise-Emagrecedores.com</title>
        <meta name="description" content="">
	    <meta name="keywords" content="">
	    <meta name="robots" content=""/>
	    <link rel="canonical" href="index.html" />

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="../img/icons/favicon.png" type="image/x-icon">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:400" rel="stylesheet">

        <link rel="stylesheet" href="../../cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <link rel='stylesheet' id='normalize-css'  href='../css/normalize.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bootstrap-css'  href='../css/bootstrap-custom.css' type='text/css' media='all' />

        <!-- CSS global -->
        <link rel='stylesheet' id='theme-css'  href='../css/style.css' type='text/css' media='all' />
        <link rel='stylesheet' id='themepage-css'  href='../css/theme_pages.css' type='text/css' media='all' />


        <script type='text/javascript' src='../../ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js'></script>
        <script type='text/javascript' src='../js/devicedetect.min.js'></script>
        <script type='text/javascript' src='../js/modernizr.custom.js'></script>
        <script type='text/javascript' src='../js/fastclick.min.js'></script>
	</head>
	<body class="page-template-default page page-id-5 contact">
    <?php
        include('../components/nav-bar.php');
    ?>
        <section id="wrapper" role="document"> 
            <div id="content" role="main">
                <article class="post-5 page type-page status-publish hentry" id="post-5">
                    <div class="container">
                        <div id="hero"></div>
                        <div id="alcf-contactform-wrap">
                            <h1>Contato</h1>
                            <form action="#" method="post" id="alcf-contactform">
                                <fieldset class="alcf-name">
                                    <label for="alcf_contactform_name">Nome</label>
                                    <input name="alcf_contactform_name" id="alcf_contactform_name" type="text" class="required" size="33" maxlength="99" value="" placeholder="Digite seu nome" />
                                </fieldset>
                                <fieldset class="alcf-email">
                                    <label for="alcf_contactform_email">Email</label>
                                    <input name="alcf_contactform_email" id="alcf_contactform_email" type="text" class="required email" size="33" maxlength="99" value="" placeholder="Digite seu email" />
                                </fieldset>
                                <fieldset class="alcf-message">
                                    <label for="alcf_message">Mensagem</label>
                                    <textarea name="alcf_message" id="alcf_message" class="required" minlength="4" cols="33" rows="7" placeholder="Digite sua mensagem"></textarea>
                                </fieldset>
                                <div class="alcf-submit">
                                    <input type="submit" name="BTEnvia" id="alcf_contact" class="button tiny radius" value="Enviar">
                                    <input type="hidden" name="alcf_key" value="process">
                                </div>
                            </form>
                        </div>
                    </div>
                </article>
                <br>
                <br>
		</section>
        <?php
        include('../components/footer.php');
        ?>
	</body>

<!-- Mirrored from analise-emagrecedores.com/contato/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 05 Nov 2018 13:11:42 GMT -->
</html>