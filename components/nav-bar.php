<?php
    $host = $_SERVER['HTTP_HOST'];

    if($host == "192.168.0.41" || $host == "localhost" )
    {
        $base_urlNav = "http://192.168.0.41/mundosuplementos/pages/comparadores/potencia-masculina.funilvenda.com";
        $base_urlHost = "http://192.168.0.41/mundosuplementos/pages/comparadores/potencia-masculina.com";
    }
    else
    {
        $base_urlNav  = "https://www.resources.potencia-masculina.com";
        $base_urlHost = "https://potencia-masculina.com";
    }
?>
<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo "{$base_urlHost}"; ?>">
                    <img src='<?php echo "{$base_urlHost}/img/potencia-masculina.svg"; ?>' title="Potencia Masculina" alt="Potencia Masculina" class="image-header">
                </a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="<?php echo "{$base_urlHost}"; ?>">TOP 3 Estimulantes</a></li>
                    <li><a href="<?php echo "{$base_urlHost}/pages/criterios-avaliacao.php"; ?>">Critérios de avaliação</a></li>
                    <li><a href="<?php echo "{$base_urlHost}/pages/sobre-nos.php"; ?>">Sobre nós</a></li>
                    <li><a href="<?php echo "{$base_urlHost}/pages/contato.php"; ?>">Contato</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>