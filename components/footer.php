<?php
$host = $_SERVER['HTTP_HOST'];
if($host == "192.168.0.41" || $host == "localhost" )
    $base_urlNav = "https://192.168.0.41/mundosuplementos/pages/comparadores/potencia-masculina.com";
else
    $base_urlNav = "https://www.potencia-masculina.com";
?>
<footer>
    <div id="footer_wrapper">
        <div id="footer_nav">
            <ul id="menu-footer-navigation" class="nav_horiz">
                <li id="menu-item-476" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-476"><a href="<?php echo "{$base_urlNav}"; ?>">TOP 3 Estimulantes</a></li>
                <li id="menu-item-476" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-4 current_page_item menu-item-476"><a href="<?php echo "{$base_urlNav}/pages/criterios-avaliacao.php"; ?>">Critérios de avaliação</a></li>
                <li id="menu-item-487" class="menu-item menu-item-type-products menu-item-object-cpt-archive menu-item-487"><a href="<?php echo "{$base_urlNav}/pages/sobre-nos.php"; ?>">Sobre nós</a></li>
                <li id="menu-item-473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-473"><a href="<?php echo "{$base_urlNav}/pages/contato.php"; ?>">Contato</a></li>
            </ul>
        </div>
        <div id="footer_subnav">
            <p class="copyright">
                © 2018 - <script type="text/javascript">var now=new Date(); document.write(now.getFullYear());</script>
                <a href="https://mundosuplementos.com.br" target="_blank">Mundo Suplementos</a>- Todos direitos reservados
            </p>
        </div>
    </div>
</footer>
<div class="screen_sizer visible-xs" data-screen-size="0"></div>
<div class="screen_sizer visible-sm" data-screen-size="1"></div>
<div class="screen_sizer visible-md" data-screen-size="2"></div>
<div class="screen_sizer visible-lg" data-screen-size="3"></div>
<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js'></script>
<script type='text/javascript' src='../js/devicedetect.min.js'></script>
<script type='text/javascript' src='../js/modernizr.custom.js'></script>
<script type='text/javascript' src='../js/fastclick.min.js'></script>
<script type="text/javascript" src="../app/ajaxGetPost.js"></script>
<script type="text/javascript" src="../app/main.js"></script>
<script type="text/javascript" src="../js/validate.js"></script>
<script type='text/javascript' src='../js/spin.min.js'></script>
<script type='text/javascript' src='../js/json3.min.js'></script>
<script type='text/javascript' src='../js/ukm.spin.min.js'></script>
<script type='text/javascript' src='../js/cookies.min.js'></script>
<script type='text/javascript' src='../js/theme-scripts.js'></script>
<script type='text/javascript' src='../js/theme_pages.js'></script>
<script type='text/javascript' src='../js/bootstrap-custom.js'></script>
<script type='text/javascipt' src='../js/browser-detect.min.js'></script>