<div class="post-author-bio">
    <div class="author-img">
        <img src="../img/elizabeth-pic.png" alt=" ">
    </div>
    <div class="author-profile">
        <p><strong>Sobre o autor:</strong> Elisa Vieira </p>
        <p>Elisa Vieira é uma redatora especialista em potencializadores que busca diariamente por informações verídicas sobre aumento do prazer e eficácia dos produtos. Ela tem cerca de 10 anos de experiência com pesquisa e análise de produtos potencializadores e o seu único objetivo é disponibilizar a informação de forma clara e organizada para que os leitores tenham condições de analisar por si mesmos e tomar decisões mais inteligentes.</p>
    </div>
</div>